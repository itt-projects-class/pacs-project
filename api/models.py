from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta
# Create your models here.



STATE = (
    ('DN', 'Done'),
    ('DG', 'Doing'),
    ('NS', 'NoStared'),
)


class GrantGoal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    goal_title = models.CharField(max_length=128)
    description = models.TextField(max_length=256)
    timestamp = models.DateField(auto_now_add=True)
    days_duration = models.IntegerField(default=7)
    final_date = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=STATE, default="None")
    sprint = models.FloatField(default=0.0)
    slug = models.SlugField(max_length=128)

    def __str__(self):
        return self.goal_title

@receiver(post_save, sender=GrantGoal)
def end_time_grantgoal(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_duration)
        instance.save()


GSTATE = (
    ('DNG', 'Done'),
    ('DGG', 'Doing'),
    ('NSG', 'NoStared'),
)

class Goal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    asigned_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='gasigned_user')## Pendiente de revisar y modificar
    grant_goal = models.ForeignKey(GrantGoal, on_delete=models.CASCADE)
    goal_name = models.CharField(max_length=128, default="Pretty Cool Goal")
    description = models.TextField(max_length=256)
    timestamp = models.DateField(auto_now_add=True)
    duration = models.IntegerField(default=1)
    final_date = models.DateField(auto_now_add=False)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=GSTATE, default="None")
    slug = models.SlugField(max_length=32)

    def __str__(self):
        return self.goal_name
    


SUBSTATE = (
    ('DNS', 'Done'),
    ('DGS', 'Doing'),
    ('NSS', 'NoStared'),
)

class SubGoal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    asigned_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sasigned_user')## Pendiente de revisar y modificar
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    subgoal_name = models.CharField(max_length=128, default="Pretty Cool SubGoal")
    description = models.TextField(max_length=256)
    timestamp = models.DateField(auto_now_add=True)
    duration = models.IntegerField(default=1)
    final_date = models.DateField(auto_now_add=False)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=SUBSTATE, default="None")
    slug = models.SlugField(max_length=32)

    def __str__(self):
        return self.subgoal_name


TSTATE = (
    ('DT', 'Done'),
    ('DGT', 'Doing'),
    ('NST', 'NoStared'),
)

class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    asigned_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasigned_user')## Pendiente de revisar y modificar
    subgoal = models.ForeignKey(SubGoal, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=128, default="Pretty Cool SubGoal")
    description = models.TextField(max_length=256)
    timestamp = models.DateField(auto_now_add=True)
    duration = models.IntegerField(default=1)
    final_date = models.DateField(auto_now_add=False)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=TSTATE, default="None")
    slug = models.SlugField(max_length=32)

    def __str__(self):
        return self.task_name
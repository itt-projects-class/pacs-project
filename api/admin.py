from django.contrib import admin

# Register your models here.
from .models import GrantGoal

@admin.register(GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = ["goal_title", "timestamp", "final_date", "slug"]
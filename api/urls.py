from django.urls import path

from api import views

app_name = "api"

urlpatterns = [
    path('ws/users/list/', views.user_list, name="ws_users"),
    path('v1/grant_goal/list', views.GrantGoalListAPIView.as_view(), name="grant_goal_list"),
    path('v1/grant_goal/detail/<int:pk>/', views.GrantGoalDetail.as_view(), name="gran_goal_detail"),

    ###v2###
    path('v2/grant_goal/list', views.GrantGoalList2.as_view(), name="grant_goal_list2"),
    path('v2/create/grantgoal/', views.CreateGrantGoal.as_view(), name="create_grantgoal"),
    path('v2/update/grantgoal/<int:pk>/', views.UpdateGrantGoal.as_view(), name="update_grantgoal"),

    #### CLIENTE URLS #####
    path('update/grantgoal/client/<int:pk>/', views.UpdateGrantGoalClient.as_view(), name="gg_update_client"),
    path('detail/grantgoal/client/<int:pk>/', views.DetailGrantGoalClient.as_view(), name="gg_detail_client"),
    path('list/grantgoal/client/', views.ListGrantGoalClient.as_view(), name="gg_list_client"),
    path('create/grantgoal/client/', views.CreateGrantGoalClient.as_view(), name="gg_create_client"),
]
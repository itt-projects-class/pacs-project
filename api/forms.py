from django import forms

from .models import GrantGoal


class CreateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "user",
            "goal_title",
            "description",
            "days_duration",
            "status",
            "state",
            "sprint",
            "slug"
        ]


class  UpdateGrantGoalClientForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "user",
            "goal_title",
            "description",
            "days_duration",
            "status",
            "state",
            "sprint",
            "slug"
        ]
   
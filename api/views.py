from django.shortcuts import render, HttpResponse, redirect
from django.core import serializers
from django.views import generic

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from django.contrib.auth.models import User

from .serializers import GrantGoalSerializer

from .models import GrantGoal
from .forms import CreateGrantGoalForm, UpdateGrantGoalClientForm

import requests
# Create your views here.

#CREATE
class CreateGrantGoal(generics.CreateAPIView):
    serializer_class = GrantGoalSerializer


#RETRIEVE
class GrantGoalList2(generics.ListAPIView):
    queryset = GrantGoal.objects.all()
    serializer_class = GrantGoalSerializer


class GrantGoalListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        data = GrantGoal.objects.all()
        data = GrantGoalSerializer(data, many=True).data
        return Response(data)
    
class GrantGoalDetail(APIView):
    def get(self, request, pk, *args, **kwargs):
        data = GrantGoal.objects.get(id=pk)
        data = GrantGoalSerializer(data).data
        return Response(data)


#UPDATE
class UpdateGrantGoal(generics.UpdateAPIView):
    serializer_class = GrantGoalSerializer
    queryset = GrantGoal.objects.all()

#DELETE



def user_list(request):
    users = User.objects.all()
    data = serializers.serialize('json', users)
    return HttpResponse(data, content_type='application/json')


##### CLIENTS #####

class CreateGrantGoalClient(generic.View):
    template_name = "api/ggcreatec.html"
    context = {}
    url = "http://localhost:8000/api/v2/create/grantgoal/"
    response = ""
    form = CreateGrantGoalForm()

   
    def get(self, request):
        self.context = {
            'form': self.form
        }
 
        return render(request, self.template_name, self.context)
    
    def post(self, request, *args, **kwargs):
        payload = {
            "goal_title": request.POST["goal_title"],
            "description": request.POST["description"],
            "days_duration": request.POST["days_duration"],  
            "status": request.POST["status"],
            "state": request.POST["state"],
            "sprint": request.POST["sprint"],
            "slug": request.POST["slug"],
            "user": request.POST["user"]
        }
        self.response = requests.post(url=self.url, data=payload)
        print(self.response)
        return redirect("/api/list/grantgoal/client")





class UpdateGrantGoalClient(generic.View):
    template_name = "api/ggupdatec.html"
    context = {}
    url_put = "http://localhost:8000/api/v2/update/grantgoal/"
    url_get = "http://localhost:8000/api/v1/grant_goal/detail/"
    response = ""
    form = UpdateGrantGoalClientForm()

    def get(self, request, pk):
        self.url_get = self.url_get + f"{pk}/"
        self.response = requests.get(url=self.url_get)
        self.context = {
            "grantgoal": self.response.json(),
            "form": self.form
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request,pk):
        self.url_put = self.url_put + f"{pk}/"
        payload = {
            "user": request.POST["user"],
            "goal_title": request.POST["goal_title"],
            "description": request.POST["description"],
            "days_duration": request.POST["days_duration"],
            "status": request.POST["status"],
            "state": request.POST["status"],
            "slug": request.POST["slug"]
        }
        self.response = requests.put(url=self.url_put, data=payload)
        print(self.response)
        print("-----", self.url_put)
        return render(request, self.template_name, self.context)



class DetailGrantGoalClient(generic.View):
    template_name = "api/ggdetailc.html"
    context = {}
    url = "http://localhost:8000/api/v1/grant_goal/detail/"


    def get(self, request, pk):
        self.url = self.url + f"{pk}/"
        grantgoal = requests.get(url=self.url)
        self.context = {
            "grant_goal": grantgoal.json()
        }
        return render(request, self.template_name, self.context)


class ListGrantGoalClient(generic.View):
    template_name = "api/gglistc.html"
    context = {}
    url = "http://localhost:8000/api/v2/grant_goal/list"


    def get(self, request):
        grantgoal = requests.get(url=self.url)
        self.context = {
            "list_grant_goal": grantgoal.json()
        }
        return render(request, self.template_name, self.context)